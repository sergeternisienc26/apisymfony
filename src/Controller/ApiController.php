<?php

namespace App\Controller;


use App\Entity\Scores;
use App\Repository\ScoresRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Validator\Validator;

class ApiController extends AbstractController
{
    /** 
     * @Route("/", name="api")
     */


    public function index(): Response
    {
        return $this->render('api/index.html.twig', [
            'controller_name' => 'ApiController',
        ]);
    }

    //=========================//
    //   ENVOI api méthode GET
    //========================// 

    /**
     * @Route("/api/scores", name="api_scores_index", methods={"GET"})
     */

    public function ApiPost(ScoresRepository $scoresRepository): Response
    {
        //==========================//
        //Processus de SERIALISATION//
        //==========================//


        //prend ttes les prop. et méthodes class scores
        // $scores = $scoresRepository->findAll();

        // $scoresNormalises = $normalizer->normalize($scores, null, ['groups' => 'scores:gamer']);
        //NORMALISATION transformation objet (complexe) $scores en tableau associatif (simple) $scoresNormalises 
        //['groups' => 'scores:gamer']->pour l'exercice prend ttes les prop. de scores

        // $jsonScores = json_encode($scoresNormalises);
        //ENCODAGE transformation tableau en texte (ici json)

        // $jsonScores = $serializer->serialize($scores, 'json', ['groups' => 'scores:gamer']);
        //SERIALIZER propre à symfony regroupe les 2 opérations précédentes

        // $reponse = new Response($jsonScores, 200, [
        //     "Content-Type" => "application/jason"
        // ]);

        // $reponse = new JsonResponse($jsonScores, 200, [], true);
        //statut 200 -> requête ok


        return $this->json($scoresRepository->findAll(), 200, [], ['groups' => 'scores:gamer']);
    }

     //=========================//
    //   ENVOI api méthode GET
    //  10 meilleurs scores
    //========================// 

    /**
     * @Route("/api/scores/best", name="api_scores_best", methods={"GET"})
     */

    public function ApiPostBest(ScoresRepository $scoresRepository): Response
    {
        

        return $this->json($scoresRepository->findByExampleFieldOlder(), 200, [], ['groups' => 'scores:gamer']);
    }



    //==========================================//
    //   Recevoir données pour api méthode POST
    //=========================================//

    /**
     *  @Route("/api/scores/store", name="api_scores_store", methods={"POST"})
     */

    public function store(Request $request, SerializerInterface $serializer, EntityManagerInterface $em, ValidatorInterface $validator)
    {
        //envoi requête recevoir (ici json) (lire contenu requêt http)
        $jsonRecu = $request->getContent();

        //essai de désérialiser l'objet sinon envoi message via catch
        try {
            //désérialisation transforme json en objet (complexe)
            $scoresRecus = $serializer->deserialize($jsonRecu, Scores::class, 'json');

            $scoresRecus->setCreateat(new \DateTime());

            //vérifie données reçus par validator symfony (Assert dans entity)
            $errors = $validator->validate($scoresRecus);

            if(count($errors) > 0) {
                return $this->json($errors, 400);
            }

            //persister l'objet puis envoi données(requête sql) vers bdd
            $em->persist($scoresRecus);
            $em->flush($scoresRecus);

            //réponse en json et statut 201 (ressource créer sur le serveur ok et nlle ressource créée)
            return $this->json($scoresRecus, 201, [], ['groups' => 'scores:gamer']);
        } catch (NotEncodableValueException $e) {
            return $this->json([
                'status' => 400,
                'message' => $e->getMessage()
            ], 400);
        }
    }
}
