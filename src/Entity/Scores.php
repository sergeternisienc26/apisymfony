<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\ScoresRepository;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
//utilisation contraintes de validation via Assert
//ex: Assert\NotBlank->vérifie que prop. non vide

/**
 * Score par Jeu
 * 
 * @ORM\Entity(repositoryClass=ScoresRepository::class)
 */
#[ApiResource]
class Scores
{
    /**
     * ID du score
     * 
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("scores:gamer")
     */
    private $id;

    /**
     * Valeur du score
     * 
     * @ORM\Column(type="integer")
     * @Groups("scores:gamer")
     * @Assert\NotBlank(message = "score ne peut être vide")
     */
    private $score;

    /**
     * Surnom du joueur
     * 
     * @ORM\Column(type="string", length=255)
     * @Groups("scores:gamer")
     * @Assert\NotBlank(message = "pseudo ne peut être vide")
     */
    private $nickname;

    /**
     * Date de création du score
     * 
     * @ORM\Column(type="date")
     * @Groups("scores:gamer")
     */
    private $createat;

    /**
     * @ORM\Column(type="integer")
     * @Groups("scores:gamer")
     */
    private $timer;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getScore(): ?int
    {
        return $this->score;
    }

    public function setScore(int
      $score): self
    {
        $this->score = $score;

        return $this;
    }

    public function getNickname(): ?string
    {
        return $this->nickname;
    }

    public function setNickname(string $nickname): self
    {
        $this->nickname = $nickname;

        return $this;
    }

    public function getCreateat(): ?\DateTimeInterface
    {
        return $this->createat;
    }

    public function setCreateat(\DateTimeInterface $createat): self
    {
        $this->createat = $createat;

        return $this;
    }

    public function getTimer(): ?int
    {
        return $this->timer;
    }

    public function setTimer(int $timer): self
    {
        $this->timer = $timer;

        return $this;
    }
}
