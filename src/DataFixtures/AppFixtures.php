<?php

namespace App\DataFixtures;

use App\Entity\Scores;
use App\Entity\User;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasher;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;

class AppFixtures extends Fixture
{
    private $hasher;
    public function __construct(UserPasswordHasherInterface $hasher) {
        $this->hasher=$hasher;
    }

    public function load(ObjectManager $manager): void
       {
    
        // create 10 scores
        for ($i = 0; $i < 20; $i++) {
            $score = new Scores();
            $score->setNickname('popaul'.$i);
            $score->setScore(mt_rand(10, 100));
            $score->setTimer(mt_rand(10, 100));
            $score->setCreateat(new \DateTimeImmutable('NOW'));
            $manager->persist($score);
        }

        //user admin
        $user = new User();
        $user->setUsername('admin');
        $password = $this->hasher->hashPassword($user, "admin");
        $user->setPassword($password);
        $user->setRoles(["ROLE_ADMIN"]);   
        $manager->persist($user);


        //user utilisateur
        $user = new User();
        $user->setUsername('popaul');
        $password = $this->hasher->hashPassword($user, "util");
        $user->setPassword($password);
        $user->setRoles(["ROLE_USER"]);   
        $manager->persist($user);

        $manager->flush();
    }
}